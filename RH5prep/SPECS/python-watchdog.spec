%if 0%{?fedora} > 12 || 0%{?rhel} > 6
%global with_python3 1
%endif

%global __python /usr/bin/python2.6
%global __python2 /usr/bin/python2.6
%global __os_install_post %{__python26_os_install_post}

%{!?python2_sitelib: %define python2_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global modname watchdog

Name:               python26-watchdog
Version:            0.8.2
Release:            1%{?dist}
Summary:            File system events monitoring

Group:              Development/Libraries
License:            ASL 2.0 and BSD and MIT
URL:                http://pypi.python.org/pypi/watchdog
Source0:            http://pypi.python.org/packages/source/w/%{modname}/%{modname}-%{version}.tar.gz
Patch0:             watchdog-remove-deps.patch
BuildRoot:          %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:          noarch

BuildRequires:      python26-devel
#BuildRequires:      pytest
#BuildRequires:      python-pytest-cov
#BuildRequires:      PyYAML >= 3.09
#BuildRequires:      python-argh >= 0.8.1
BuildRequires:      python26-pathtools >= 0.1.1

%if 0%{?with_python3}
BuildRequires:      python3-devel
BuildRequires:      python3-pytest
BuildRequires:      python3-pytest-cov
BuildRequires:      python3-PyYAML >= 3.09
BuildRequires:      python3-argh >= 0.8.1
BuildRequires:      python3-pathtools >= 0.1.1
%endif

%description
A Python API and shell utilities to monitor file system events.

%if 0%{?with_python3}
%package -n python3-watchdog
Summary:            Filesystem events monitoring
Group:              Development/Libraries

%description -n python3-watchdog
A Python API and shell utilities to monitor file system events.

%endif

%prep
%setup -q -n %{modname}-%{version}
%patch0 -p1

# Remove all shebangs
find src -name "*.py" | xargs sed -i -e '/^#!\//, 1d'

# Remove +x of the README file
chmod -x README.rst

# Remove bundled egg-info in case it exists
rm -rf %{modname}.egg-info
%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
%{__python2} setup.py build
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif


%install
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root=%{buildroot}
mv %{buildroot}/%{_bindir}/watchmedo %{buildroot}/%{_bindir}/watchmedo-py3
popd
%endif
%{__python2} setup.py install -O1 --skip-build --root=%{buildroot}

%check
# Tests are no currently included in the releases
# https://github.com/gorakhargosh/watchdog/pull/232

#%{__python2} setup.py test
#%if 0%{?with_python3}
#pushd %{py3dir}
#%{__python3} setup.py test
#popd
#%endif

%files
%doc README.rst LICENSE
%{python2_sitelib}/%{modname}/
%{python2_sitelib}/%{modname}-%{version}*
%{_bindir}/watchmedo

%if 0%{?with_python3}
%files -n python3-watchdog
%doc README.rst LICENSE
%{python3_sitelib}/%{modname}/
%{python3_sitelib}/%{modname}-%{version}-*
%{_bindir}/watchmedo-py3

%endif

%changelog
* Sat Nov 22 2014 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.8.2-1
- Update to 0.8.2

* Fri Apr 25 2014 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.7.1-4
- Adjust the license tag to ASL2.0 and BSD and MIT

* Fri Apr 18 2014 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.7.1-3
- Adjust the check for Fedora/RHEL release number for the py3 package

* Fri Apr 18 2014 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.7.1-2
- Remove all shebang of the python files

* Fri Apr 18 2014 Pierre-Yves Chibon <pingou@pingoured.fr> - 0.7.1-1
- initial package for Fedora
