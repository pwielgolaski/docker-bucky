%global __python /usr/bin/python2.6
%global __os_install_post %{__python26_os_install_post}

%{!?python_sitelib: %define python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%global upname pathtools

Name:		python26-%{upname}
Version:	0.1.2
Release:	3%{?dist}
Summary:	Pattern matching and various utilities for file systems paths

License:	MIT
Group:         Development/Languages
URL:		https://github.com/gorakhargosh/%{upname}
Source0:	https://pypi.python.org/packages/source/p/%{upname}/%{upname}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:	noarch
BuildRequires:	python26-devel
BuildRequires:	python26-distribute

%if 0%{?fedora} || 0%{?rhel} >= 7
BuildRequires:	python-sphinx-theme-flask

BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-sphinx-theme-flask
%endif

%description
%{name} is a Python API library for common path
and pattern functionality.


%if 0%{?fedora} || 0%{?rhel} >= 7
%package -n python3-%{upname}
Summary:	Pattern matching and various utilities for file systems paths

%description  -n python3-%{upname}
%{name} is a Python API library for common path
and pattern functionality.
%endif


%prep
%setup -qn %{upname}-%{version}

# remove hashbang from lib's files
sed -i -e '/#!\//d' pathtools/*.py

# Python3 for Fedora and EL7+,
# no autodocs for RHEL <= 6 , but a hint
%if 0%{?fedora} || 0%{?rhel} >= 7
rm -rf %{py3dir} py3_docs
cp -a . %{py3dir}
%else
cat > README.docs << EOF
The version of python-sphinx in RHEL <= 6 is much too dated for building
this packages documentation.  Even python-sphinx 1.0.8 fails to build them.
If you need some good documentation and API reference, please visit:

http://pythonhosted.org/pathtools/
EOF
%endif


%build
%{__python} setup.py build

%if 0%{?fedora} || 0%{?rhel} >= 7
pushd docs
make html
rm -rf build/html/.build*
popd

pushd %{py3dir}
%{__python3} setup.py build
pushd docs
make html
rm -rf build/html/.build*
popd
%endif


%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%if 0%{?fedora} || 0%{?rhel} >= 7
mv %{py3dir}/docs py3_docs
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root %{buildroot}
%endif


%check
%{__python} setup.py test

%if 0%{?fedora} || 0%{?rhel} >= 7
pushd %{py3dir}
%{__python3} setup.py test
%endif


%files
%doc AUTHORS LICENSE README
%{?el6:%doc README.docs}
%{python_sitelib}/*

%if 0%{?fedora} || 0%{?rhel} >= 7
%files -n python3-%{upname}
%doc AUTHORS LICENSE README py3_docs/build/html
%{python3_sitelib}/*
%endif


%changelog
* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.1.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Wed May 14 2014 Bohuslav Kabrda <bkabrda@redhat.com> - 0.1.2-2
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4

* Mon Aug 12 2013 Björn Esser <bjoern.esser@gmail.com> - 0.1.2-1
- Initial RPM release (#996088)
