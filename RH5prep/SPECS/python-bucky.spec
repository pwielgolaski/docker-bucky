%global __python /usr/bin/python2.6
%global __os_install_post %{__python26_os_install_post}

%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}

Name:           python26-bucky
Version:        2.2.2
Release:        2%{?dist}
Summary:        CollectD and StatsD adapter for Graphite

Group:          System Environment/Daemons
License:        ASL 2.0
URL:            http://pypi.python.org/pypi/bucky

Source0:        http://pypi.python.org/packages/source/b/bucky/bucky-%{version}.tar.gz
Source1:        python-bucky-example.conf
Source2:        python-bucky-supervisord-example.conf
Patch0:         bucky-ignore_setproctitle.patch

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:      noarch
BuildRequires:  python26-devel, python26-distribute

Requires:       collectd, python26-distribute, python26-watchdog

%description
Bucky is a small server for collecting and translating metrics for
Graphite. It can current collect metric data from CollectD daemons
and from StatsD clients.

%prep
%setup -q -n bucky-%{version}
%patch0 -p1
%{__install} -m 644 %{SOURCE2} .


%build
%{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root %{buildroot}
%{__mkdir_p} %{buildroot}%{_localstatedir}/log/bucky
%{__mkdir_p} %{buildroot}%{_localstatedir}/run/bucky
%{__mkdir_p} %{buildroot}%{_sysconfdir}/bucky
%{__install} -m 644 %{SOURCE1} %{buildroot}%{_sysconfdir}/bucky/bucky.conf


%pre
getent group bucky >/dev/null || groupadd -r bucky
getent passwd bucky >/dev/null || \
    useradd -r -g bucky -d / \
    -s /sbin/nologin -c "Bucky daemon" bucky


%postun
if [ $1 = 0 ]; then
  getent passwd bucky >/dev/null && \
      userdel -r bucky 2>/dev/null
fi


%files
%doc THANKS README.rst LICENSE python-bucky-supervisord-example.conf
%{_bindir}/bucky
%attr(-,bucky,bucky) %{_localstatedir}/log/bucky
%attr(-,bucky,bucky) %{_localstatedir}/run/bucky
%config(noreplace) %{_sysconfdir}/bucky/bucky.conf
%{python_sitelib}/*


%changelog
* Sat Nov 22 2014 Jonathan Steffan <jsteffan@fedoraproject.org> - 2.2.2-2
- Add python-watchdog requirement
- Add python-setproctitle requirement

* Sat Nov 22 2014 Jonathan Steffan <jsteffan@fedoraproject.org> - 2.2.2-1
- Update to 2.2.2

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.6-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Tue Sep 24 2013 Jonathan Steffan <jsteffan@fedoraproject.org> - 0.2.6-3
- Update requires (RHBZ#953834), adding python-setuptools

* Thu Sep 19 2013 Jonathan Steffan <jsteffan@fedoraproject.org> - 0.2.6-2
- Update requires (RHBZ#953834)

* Tue Sep 17 2013 Jonathan Steffan <jsteffan@fedoraproject.org> - 0.2.6-1
- Update to 0.2.6

* Sun Aug 04 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Wed Mar 13 2013 Jonathan Steffan <jsteffan@fedoraproject.org> - 0.2.4-1
- Update to 0.2.4

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Sat Nov 24 2012 Jonathan Steffan <jsteffan@fedoraproject.org> - 0.2.3-1
- Update to 0.2.3

* Sat Aug 18 2012 Jonathan Steffan <jsteffan@fedoraproject.org> - 0.2.2-1
- Update to 0.2.2

* Mon May 14 2012 Jonathan Steffan <jsteffan@fedoraproject.org> - 0.1.0-1
- Initial package
