#!/bin/sh
NAME="pwielgolaski/bucky6.prep"
docker build -t $NAME -f Dockerfile.prep .
docker run --rm -v $(pwd)/rpms/:/rpms -t $NAME /bin/bash -c "cp /root/rpmbuild/RPMS/noarch/*.rpm /rpms/"

NAME="pwielgolaski/bucky6"
docker build -t $NAME .
docker run --rm -v $(pwd)/all/:/all -t $NAME /bin/bash -c "cp -R /rpms/ /all/"
